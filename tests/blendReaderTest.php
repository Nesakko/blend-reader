<?php
/**
 * Copyright (C) 2013-2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class blendReaderTest extends TestCase {
	/**
	 * @var \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlenderBinary
	 */
    private $reader;
    
    protected function setUp(): void {
        $log = new Logger('blend-reader');
        $log->pushHandler(new StreamHandler('/tmp/blend-reader.log', Logger::DEBUG));

        $this->reader = new \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlenderBinary($log, '/opt/blender2.83/rend.exe', '/opt/blender2.93/rend.exe', '/opt/blender3.0.0/rend.exe');
    }
    
    public function testFileDoesNotExist() {
        $blender_file = __DIR__.'/'.time().'.blend';
        
        while (file_exists($blender_file)) {
            $blender_file .= '.blend';
        }
        
        $ret = $this->reader->open($blender_file);
        $this->assertFalse($ret);
    }
    
    public function testNotABlenderFile() {
        $blender_file = __DIR__.'/data/000000.png';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertFalse($infos);
    }
    
    public function test293cycles1920x108040pcstart10end100() {
        $blender_file = __DIR__.'/data/293-cycles-1920x1080-40pc-start10-end100.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(10, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(1920 * 1080 * 0.40 * 0.40 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test293cycles200x100100pcstart1end250compressed() {
        $blender_file = __DIR__.'/data/293-cycles-200x100-100pcstart1-end250-compressed.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(250, $infos['end_frame']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(200, $infos['resolution_x']);
        $this->assertEquals(100, $infos['resolution_y']);
        $this->assertEquals((int)(200 * 100 * 1 * 1 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test293cycles530x260100pcstart1end100() {
        $blender_file = __DIR__.'/data/293-cycles-530x260-100pc-start1-end100.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 1 * 1 * 150), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test293cycles530x26050pcstart1end100SquareSamples() {
        $blender_file = __DIR__.'/data/293-cycles-530x260-50pc-start1-end100-square-samples.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithQuote() {
        $blender_file = __DIR__."/data/path-with-'quote'-293-cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithSpace() {
        $blender_file = __DIR__."/data/path-with- -293-cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test293cycles24fps() {
        $blender_file = __DIR__.'/data/293-cycles-24fps.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('framerate', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertTrue($infos['have_camera']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(24, $infos['framerate']);
    }
    
    public function testUnusedEnginesInCompositor() {
        $blender_file = __DIR__.'/data/290_renderEngineTest.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertFalse(array_key_exists('cycles_samples', $infos));

        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }

    public function testMixedEnginesInCompositor() {
        $blender_file = __DIR__.'/data/290_renderEngineTestMixed.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertFalse(array_key_exists('cycles_samples', $infos));

        $this->assertEquals('MIXED', $infos['engine']);
    }

    public function testBlender293CanUseTileCyclesWithCompositing() {
        $blender_file = __DIR__.'/data/293-cycles-compositing.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function testBlender293CanUseTileCyclesWithoutCompositing() {
        $blender_file = __DIR__.'/data/293-cycles-no-compositing.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender293CanUseTileCycleCompositingDisabled() {
        $blender_file = __DIR__.'/data/293-cycles-compositing-disabled.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender293CanUseTileCycleCompositingEnableUseNodeDisabled() {
        $blender_file = __DIR__.'/data/293-cycles-compositing-disabled-use-node-disabled.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender293CanUseTileCyclesWithoutDenoising() {
        $blender_file = __DIR__.'/data/293-cycles-no-denoising.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender293CanUseTileCycleDenoising() {
        $blender_file = __DIR__.'/data/293-cycles-denoising.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender293', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function test280Cycles() {
        $blender_file = __DIR__.'/data/280-cycles.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }
    
    public function test280Eevee() {
        $blender_file = __DIR__.'/data/280-eevee.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }
    
    public function test280Workbench() {
        $blender_file = __DIR__.'/data/280-workbench.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('BLENDER_WORKBENCH', $infos['engine']);
    }
    
    public function test290Cycles() {
        $blender_file = dirname(__FILE__).'/data/290-cycles.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        
        $this->assertEquals('blender290', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals('.png', $infos['output_file_extension']);
    }
    
    public function test290Eevee() {
        $blender_file = dirname(__FILE__).'/data/290-eevee.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender290', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }
    
    public function testBlender280CanUseTileCyclesWithCompositing() {
        $blender_file = __DIR__.'/data/280-cycles-compositing.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function testBlender280CanUseTileCyclesWithoutCompositing() {
        $blender_file = __DIR__.'/data/280-cycles-no-compositing.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender280CanUseTileCycleCompositingDisabled() {
        $blender_file = __DIR__.'/data/280-cycles-compositing-disabled.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender280CanUseTileCycleCompositingEnableUseNodeDisabled() {
        $blender_file = __DIR__.'/data/280-cycles-compositing-disabled-use-node-disabled.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender280CanUseTileCyclesWithoutDenoising() {
        $blender_file = __DIR__.'/data/280-cycles-no-denoising.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender280CanUseTileCycleDenoising() {
        $blender_file = __DIR__.'/data/280-cycles-denoising.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function testMissingFile() {
        $blender_file = __DIR__.'/data/282-missing-files.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        
        $this->assertTrue(is_array($infos['missing_files']));
        $this->assertEquals(1, count($infos['missing_files']));
        $this->assertEquals('//../home/laurent/sheepit-contour.png', $infos['missing_files'][0]);
    }
    
    public function testDetectsActiveFileOutputNodes() {
        $blender_file = __DIR__.'/data/creating-images-outside-of-working-dir.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertTrue($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesFalse() {
        $blender_file = dirname(__FILE__).'/data/280-cycles-denoising.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertFalse($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesInActiveScenes() {
        $blender_file = __DIR__.'/data/292-output_file_node.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertTrue($infos['has_active_file_output_node']);
    }

    public function testDetectsActiveFileOutputNodesInActiveScenesMuted() {
        $blender_file = __DIR__.'/data/292-output_file_node_muted.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertFalse($infos['has_active_file_output_node']);
    }

    public function testDetectsGPencilObjects() {
        $blender_file = __DIR__.'/data/290_GPencil_detection.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_GPencil_object', $infos));
        $this->assertTrue($infos['has_GPencil_object']);
    }

    public function testDetectsGPencilObjectsFalse() {
        $blender_file = __DIR__.'/data/290_default_scene.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_GPencil_object', $infos));
        $this->assertFalse($infos['has_GPencil_object']);
    }

    public function testAviJpegOutput() {
        $blender_file = __DIR__.'/data/283.avi.jpg.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender283', $infos['version']);
        $this->assertEquals('AVI_JPEG', $infos['output_file_format']);
    }

    public function testAviRawOutput() {
        $blender_file = __DIR__.'/data/283.avi.raw.blend';
    
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
    
        $infos = $this->reader->getInfos();
    
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
    
        $this->assertEquals('blender283', $infos['version']);
        $this->assertEquals('AVI_RAW', $infos['output_file_format']);
    }
    
    public function testFFmpegOutput() {
        $blender_file = __DIR__.'/data/283.ffmpeg.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender283', $infos['version']);
        $this->assertEquals('FFMPEG', $infos['output_file_format']);
    }
    
    public function testPNGOutput() {
        $blender_file = __DIR__.'/data/280-eevee.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender280', $infos['version']);
        $this->assertEquals('PNG', $infos['output_file_format']);
    }
    
    public function teststereo3dAnglyph() {
        $blender_file = __DIR__.'/data/290-stereo3d-anglyph.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    public function teststereo3dInterlace() {
        $blender_file = __DIR__.'/data/290-stereo3d-interlace.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dSideBySide() {
        $blender_file = __DIR__.'/data/290-stereo3d-side-by-side.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300 * 2, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dTopBottom() {
        $blender_file = __DIR__.'/data/290-stereo3d-top-bottom.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200 * 2, $infos['resolution_y']);
    }
    
    public function test290IsOptixEnabled() {
        $blender_file = __DIR__.'/data/290-OptiXDenoiserEnabled.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));

        $this->assertTrue(array_key_exists('optix_is_active', $infos));
        $this->assertTrue($infos['optix_is_active']);
    }

    public function test291IsOptixEnabled() {
        $blender_file = __DIR__.'/data/291-OptiXDenoiserEnabled.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));

        $this->assertTrue(array_key_exists('optix_is_active', $infos));
        $this->assertTrue($infos['optix_is_active']);
    }
    
    public function testGetVersion280() {
		$blender_file = __DIR__.'/data/280-cycles.blend';
	
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
	
		$this->assertEquals('280', $this->reader->getVersion());
	}
	
	public function testGetVersion291() {
		$blender_file = __DIR__.'/data/291-OptiXDenoiserEnabled.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$this->assertEquals('291', $this->reader->getVersion());
	}
	
	public function testGetVersion300() {
		$blender_file = __DIR__.'/data/300-cycles.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$this->assertEquals('300', $this->reader->getVersion());
	}
	
	public function testGetVersionNotBlend() {
		$blender_file = __DIR__.'/data/000000.png';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$this->assertEquals('', $this->reader->getVersion());
	}
	
    public function testGetVersionTwice() {
        $blender_file = __DIR__.'/data/280-cycles.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $this->assertEquals('280', $this->reader->getVersion());
        // get version should reset the read to begining of the file
        $this->assertEquals('280', $this->reader->getVersion());
    }
	
	public function testBlender300Info() {
		$blender_file = __DIR__.'/data/300-cycles.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$infos = $this->reader->getInfos();
		
		$this->assertTrue(is_array($infos));
		$this->assertTrue(array_key_exists('version', $infos));
		$this->assertTrue(array_key_exists('engine', $infos));
		$this->assertTrue(array_key_exists('can_use_tile', $infos));
		
		$this->assertEquals('blender300', $infos['version']);
		$this->assertEquals('CYCLES', $infos['engine']);
		$this->assertFalse($infos['can_use_tile']); // It is False. The .blend uses Denoising
	}
	
	public function testBlender300Compress() {
		$blender_file = __DIR__.'/data/300-compress.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$infos = $this->reader->getInfos();
		
		$this->assertTrue(is_array($infos));
		$this->assertTrue(array_key_exists('version', $infos));
		$this->assertTrue(array_key_exists('engine', $infos));
		
		$this->assertEquals('300', $infos['version']);
		$this->assertEquals('BLENDER_EEVEE', $infos['engine']);
		$this->assertFalse($infos['can_use_tile']);
	}
}
