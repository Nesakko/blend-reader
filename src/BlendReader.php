<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace SheepItRenderFarm\BlendReader;

abstract class BlendReader {
    
    /**
     * Open a blend file
     * @return bool
     */
    public abstract function open($path);
    
    /**
     * Get blend's infos,
     * return should be an array at least these info:
     * info = {}
     * info['scene']
     * info['start_frame']
     * info['end_frame']
     * info['output_file_extension']
     * info['engine']
     * info['resolution_percentage']
     * info['resolution_x']
     * info['resolution_y']
     * info['framerate']
     * info['missing_files']
     * info['have_camera']
     * info['scripted_driver']
     *
     * @return string[]
     *
     */
    public abstract function getInfos();
    
    /**
     * Get Blend version,
     * blend 2.80 will return 280
     * Not blend file will return empty string
     */
    public abstract function getVersion(): String;
}
